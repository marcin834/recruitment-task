﻿Cześć,

Przygotowałem dla Ciebie dwa taski:

1. W katalogu /1/ znajdziesz plik z arrayem oraz index. Chciałbym, aby z pliku array.php została pobrana tablica Ajaxem i wyświetlona na indexie.
2. W katalogu /2/ znajduje się plik PSD, chciałbym, abyś zakodował ten mały widok z użyciem SCSS (nie musi być RWD), przy okazji, chciałbym abyś zainstalował i skonfigurował gulp (w komicie powinny znaleźć się pliki gulpfile.js oraz package.json).

Na zadanie masz 4h, czyli 17:00 - 21:00.

Powodzenia! :)